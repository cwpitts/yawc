MODULE      := yawc
ENV_NAME    := yawc
CONDA       := source ~/anaconda3/etc/profile.d/conda.sh; conda activate ${ENV_NAME}
SHELL       := bash

test:
	(${CONDA}; \
	PYTHONPATH=.:${MODULE} py.test --verbose)

coverage:
	(${CONDA}; \
	PYTHONPATH=.:${MODULE} py.test --verbose --cov=${MODULE} --cov-report=term-missing)

format:
	(${CONDA}; \
	find ${MODULE}/ -name '*.py' -exec isort {} \; ; \
	black ${MODULE}/)
