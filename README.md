# Yet Another Wordle Clone

YAWC is a clone of the popular Wordle game, to be used as an example
of programming in Raylib (specifically with the Python bindings).

## Requirements
- Python 3.8+
- raylib
- pytest
- pytest-cov

## Acknowledgements
- Wordlist sources:
  - `default`: [Wordle itself](http://www.powerlanguage.co.uk/wordle)
  - `hard`: [Keith Vertanen](https://www.keithv.com/software/wlist/)
  - `ethereum`: [OpenEthereum project](https://github.com/OpenEthereum/wordlist/blob/master/res/wordlist.txt)
  - `ogden`: [Ogden's Basic English](http://ogden.basic-english.org/wordalph.html)
  - `simple`: [Simple English Wiktionary](https://simple.wiktionary.org/wiki/User:AKA_MBG/English_Simple_Wikipedia_20080214_freq_wordlist)
  - `lawler`: [John Lawler at UMich](http://www-personal.umich.edu/~jlawler/wordlist.html)
