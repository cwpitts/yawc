"""Various constant values.

"""
from pathlib import Path
from typing import Tuple

import pyray

FONT_SIZE: int = 32
TILE_SIZE: int = 75
TILE_COLOR_NOT_FINAL: Tuple[int] = pyray.LIGHTGRAY
TILE_TEXT_COLOR: Tuple[int] = pyray.BLACK
TILE_LETTER_FONT_SIZE: int = 32
FONT_SCALING: int = 1
DATA_DIR: Path = Path(__file__).parent / "data"
