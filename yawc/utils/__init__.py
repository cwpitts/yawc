from typing import Dict

import pyray

key_to_str: Dict[int, str] = {
    pyray.KEY_A: "A",
    pyray.KEY_B: "B",
    pyray.KEY_C: "C",
    pyray.KEY_D: "D",
    pyray.KEY_E: "E",
    pyray.KEY_F: "F",
    pyray.KEY_G: "G",
    pyray.KEY_H: "H",
    pyray.KEY_I: "I",
    pyray.KEY_J: "J",
    pyray.KEY_K: "K",
    pyray.KEY_L: "L",
    pyray.KEY_M: "M",
    pyray.KEY_N: "N",
    pyray.KEY_O: "O",
    pyray.KEY_P: "P",
    pyray.KEY_Q: "Q",
    pyray.KEY_R: "R",
    pyray.KEY_S: "S",
    pyray.KEY_T: "T",
    pyray.KEY_U: "U",
    pyray.KEY_V: "V",
    pyray.KEY_W: "W",
    pyray.KEY_X: "X",
    pyray.KEY_Y: "Y",
    pyray.KEY_Z: "Z",
}
