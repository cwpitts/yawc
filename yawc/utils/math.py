"""Math utility functions.

"""
from typing import Tuple

import constants
import pyray


def compute_letter_position(
    font, letter: str, tile_corner: Tuple[float, float], tile_size: int
) -> Tuple[int, int]:
    x, y = tile_corner
    letter_size: int = pyray.measure_text_ex(font, letter, constants.FONT_SIZE, 1)
    letter_offset_x: int = letter_size.x / 2
    letter_offset_y: int = letter_size.y / 2
    return x + letter_offset_x + tile_size / 4, y + letter_offset_y + tile_size / 4
