"""Wordle clone.

YAWC is Yet Another Wordle Clone, a replica
of the viral word game.

"""
import logging
from argparse import ArgumentParser, Namespace
from pathlib import Path
from pprint import pformat
from typing import Dict, List, Tuple

import constants
import pyray
from logging_config import log_fmt
from model import GameState, LetterInfo, Model
from pyray import Camera2D, Font, Vector2
from utils import key_to_str
from utils.math import compute_letter_position

pyray.set_trace_log_level(logging.FATAL)

log: logging.Logger = logging.getLogger("yawc:__main__")


def valid_int(value: int) -> int:
    """Enforce restrictions on the integer range.

    Args:
      value (int): Value to validate

    Returns:
      value (int): Validated value

    Raises:
      ValueError:
        When the value is not an integer, or is outside the valid range

    """
    if not isinstance(value, int):
        value = int(value)

    if value < 1:
        raise ValueError(f"Invalid value {value}")


def existing_wordlist(name: str) -> Path:
    """Validate existing wordlist path.

    Args:
      name (str): Name of wordlist

    Returns:
      path (Path): Validated and resolved path to wordlist

    Raises:
      ValueError:
        When the path does not exist or could not be converted to a Path object

    """
    path: Path = (constants.DATA_DIR / name).with_suffix(".txt")

    path = path.resolve()

    if not path.exists():
        raise ValueError(f"Path not found at {path}")

    return path


def parse_args(argv: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      argv (List[str], optional):
        Program argument vector as supplied by the OS, defaults to None

    Returns:
      args (Namespace): Parsed and validated arguments

    Notes:
      Using the default `None` value for the `argv` parameter will cause
      sys.stdin to be used as the source of the argv.

    """
    argp: ArgumentParser = ArgumentParser(prog="yawc")
    argp.add_argument(
        "-W",
        "--wordlist",
        help="name of wordlist",
        default="default",
        type=existing_wordlist,
    )
    argp.add_argument(
        "--word-length", help="length of word", default=5, type=valid_int,
    )
    argp.add_argument(
        "-L",
        "--log-level",
        help="logging level",
        default="INFO",
        choices=["DEBUG", "INFO"],
    )
    argp.add_argument(
        "--log-file", help="path to log file", default=None, type=Path,
    )
    argp.add_argument(
        "-Q", "--quiet", help="run in quiet mode", default=False, action="store_true",
    )

    args: Namespace = argp.parse_args(argv)
    args.log_level = getattr(logging, args.log_level)

    return args


def draw_model(model: Model, font: Font = pyray.get_font_default()):
    """Draw model.

    Args:
      model (Model): Model to draw
      font (Font): Font to draw with

    Notes:
      This method can only be used if drawing has been started.

    """
    guess_infos: List[
        List[Tuple[bool, Tuple[str, LetterInfo]]]
    ] = model.get_guess_info()
    dx: int = -len(guess_infos[0][1] * constants.TILE_SIZE) // 2
    dy: int = -len(guess_infos * constants.TILE_SIZE) // 2
    for is_final, guess_info in guess_infos:
        for letter, letter_info in guess_info:
            if not is_final:
                tile_color: Tuple[int] = constants.TILE_COLOR_NOT_FINAL
            else:
                tile_color: Tuple[int] = letter_info.console_color()
            pyray.draw_rectangle(
                dx, dy, constants.TILE_SIZE, constants.TILE_SIZE, tile_color
            )

            letter_x, letter_y = compute_letter_position(
                font, letter, (dx, dy), constants.TILE_SIZE
            )
            pyray.draw_text_ex(
                font,
                letter,
                Vector2(int(letter_x), int(letter_y)),
                constants.TILE_LETTER_FONT_SIZE,
                constants.FONT_SCALING,
                constants.TILE_TEXT_COLOR,
            )

            dx += constants.TILE_SIZE + 10

        dx = -len(guess_infos[0][1] * constants.TILE_SIZE) // 2
        dy += constants.TILE_SIZE + 10


def draw_alphabet_from_model(model: Model, font: Font = pyray.get_font_default()):
    """Draws alphabet based on current and past guesses.

    Args:
      model (Model): Model with guesses

    Notes:
      This method can only be used if drawing has been started.

    """
    guess_infos: List[
        List[Tuple[bool, Tuple[str, LetterInfo]]]
    ] = model.get_guess_info()

    letter_states: Dict[str, LetterInfo] = {
        letter: LetterInfo.NOT_IN_WORD for letter in key_to_str.values()
    }
    letter_states: Dict[str, LetterInfo] = {}

    for is_final, guess_info in guess_infos:
        if not is_final:
            continue
        for letter, letter_info in guess_info:
            letter_states[letter] = min(
                letter_states.get(letter, LetterInfo.NOT_IN_WORD), letter_info
            )

    dx: int = -len(guess_infos[0][1] * constants.TILE_SIZE) // 2 - (
        2 * constants.TILE_SIZE
    )
    dy: int = -len(guess_infos * constants.TILE_SIZE) // 2
    letter_size: Vector2 = pyray.measure_text_ex(
        pyray.get_font_default(), "A", constants.TILE_LETTER_FONT_SIZE, 1
    )
    for idx, letter in enumerate(key_to_str.values()):
        if letter in letter_states:
            letter_color: Tuple[int] = letter_states[letter].console_color()
        else:
            letter_color: Tuple[int] = pyray.LIGHTGRAY

        pyray.draw_text_ex(
            font,
            letter,
            Vector2(int(dx), int(dy)),
            constants.TILE_LETTER_FONT_SIZE,
            constants.FONT_SCALING,
            letter_color,
        )

        dx += letter_size.x * 1.5
        if (idx + 1) % 5 == 0:
            dy += letter_size.y * 1.5
            dx = -len(guess_infos[0][1] * constants.TILE_SIZE) // 2 - (
                2 * constants.TILE_SIZE
            )


def main(args: Namespace = None):
    """Main program logic

    Args:
      args (Namespace): Program arguments

    """
    if args is None:
        args = parse_args()

    if not args.quiet:
        log.setLevel(args.log_level)
        if args.log_file is not None:
            handler: logging.Handler = logging.FileHandler(args.log_file)
        else:
            handler: logging.Handler = logging.StreamHandler()
        handler.setFormatter(log_fmt)
        log.addHandler(handler)

    log.debug("Running with args:\n%s", pformat(vars(args)))

    model: Model = Model(args.wordlist)

    pyray.init_window(800, 600, "YAWC")
    camera = Camera2D(
        Vector2(pyray.get_screen_width() / 2, pyray.get_screen_height() / 2),
        Vector2(0, 0),
        0,
        constants.FONT_SCALING,
    )

    font: Font = pyray.load_font(str(constants.DATA_DIR / "fira-code.ttf"))

    while not pyray.window_should_close():
        if model.game_state() == GameState.PLAYING:
            # Update model via keypress
            for key, value in key_to_str.items():
                if pyray.is_key_pressed(key):
                    model.add_to_current_guess(value)
            if pyray.is_key_pressed(pyray.KEY_BACKSPACE):
                model.trim_current_guess()
            if not model.current_guess().is_final and pyray.is_key_pressed(
                pyray.KEY_ENTER
            ):
                model.finalize_current_guess()
        else:
            # Reset if the "R" key is pressed
            if pyray.is_key_pressed(pyray.KEY_R):
                model.reset()

        # Render model
        pyray.begin_drawing()
        pyray.clear_background(pyray.BLACK)

        # Draw guesses
        pyray.begin_mode_2d(camera)

        draw_model(model, font)
        draw_alphabet_from_model(model, font)

        if model.game_state() != GameState.PLAYING:
            n_guesses: int = model.current_guess().size
            x: int = -(n_guesses * constants.TILE_SIZE) // 2
            y: int = -(n_guesses * constants.TILE_SIZE) // 2 - constants.TILE_SIZE

            if model.game_state() == GameState.LOSE:
                state_msg: str = f"You lose! The word was: {model.word}. [R to restart]"
                state_msg_color: Tuple[int] = pyray.RED
            elif model.game_state() == GameState.WON:
                state_msg: str = "You win! [R to restart]"
                state_msg_color: Tuple[int] = pyray.GREEN

            text_size: int = pyray.measure_text_ex(
                font, state_msg, constants.TILE_LETTER_FONT_SIZE, constants.FONT_SCALING
            )
            pyray.draw_text_ex(
                font,
                state_msg,
                Vector2(x - text_size.x // 4, y - text_size.y // 2),
                constants.TILE_LETTER_FONT_SIZE,
                constants.FONT_SCALING,
                state_msg_color,
            )

        pyray.end_mode_2d()

        pyray.end_drawing()


if __name__ == "__main__":
    _args: Namespace = parse_args()
    main(_args)
