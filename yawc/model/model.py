"""Core game model.

What does the model have to keep track of?

- The word
- The list of guesses
- The time

"""
import logging
import random
from copy import deepcopy
from enum import Enum, auto
from pathlib import Path
from typing import List, Tuple

from logging_config import log_fmt

log: logging.Logger = logging.getLogger("yawc:model:model")

from .guess import Guess, LetterInfo


class GameState(Enum):
    PLAYING = auto()
    WON = auto()
    LOSE = auto()


class Model:
    """Game model.

    Holds the word tiles and the guess.

    """

    def __init__(self, wordlist_path: Path, word_length: int = 5):
        """Model.

        Args:
          wordlist_path (Path): Path to list of words
          word_length (int, optional): Length of word to filter on, defaults to 5

        """
        self._word_length: int = word_length
        self._wordlist: List[str] = self._filter_words(wordlist_path, self._word_length)
        self._word: str = random.choice(self._wordlist)
        self._guesses: List[Guess] = [
            Guess(self._word_length) for _ in range(self._word_length + 1)
        ]

    def reset(self):
        """Reset model.

        Resets the models to an new word and empty guesses.

        """
        self._word = random.choice(self._wordlist)
        self._guesses = [Guess(self._word_length) for _ in range(self._word_length + 1)]

    def _filter_words(self, wordlist_path: Path, word_length: int) -> List[str]:
        """Filter words by length.

        Args:
          wordlist_path (Path): Path to list of words
          word_length (int): Length of word to filter on

        Returns:
          words (List[str]): List of words filtered by length

        """
        words: List[str] = []

        with open(wordlist_path, "r") as in_file:
            for word in in_file.read().strip().split("\n"):
                if len(word) == word_length and all(c.isalpha() for c in word):
                    words.append(word.upper())

        return words

    def add_to_current_guess(self, letter: str):
        """Add to current guess buffer.

        Args:
          letter (str): Letter to add

        """
        current_guess: Guess = self._guesses[self._find_current_guess_index()]
        current_guess.add_to_buffer(letter)

    def trim_current_guess(self):
        """Remove a word from current guess buffer.
        """
        current_guess: Guess = self._guesses[self._find_current_guess_index()]
        current_guess.trim_buffer()

    def _find_current_guess_index(self) -> int:
        """Find the index of the current guess.

        Returns:
          idx (int):
            The integer index of the current guess if there is one, -1
            otherwise

        """
        for idx, guess in enumerate(self._guesses):
            if not guess.is_final:
                return idx

        return -1

    def current_guess(self) -> Guess:
        """Return copy of current guess.

        Returns:
          Guess: A deep copy of the current guess

        """
        return deepcopy(self._guesses[self._find_current_guess_index()])

    def get_guess_info(self) -> List[Tuple[bool, List[Tuple[str, LetterInfo]]]]:
        """Return list of LetterInfo objects.

        Returns:
          List[List[LetterInfo]]: Deep copy of the current guesses

        """
        return [(g.is_final, g.compare_to_word(self._word)) for g in self._guesses]

    def finalize_current_guess(self):
        """Finalize current guess.

        """
        current_guess: Guess = self._guesses[self._find_current_guess_index()]
        if not current_guess.buffer_str() in self._wordlist:
            return
        else:
            current_guess.finalize()

    @property
    def guesses(self) -> List[Guess]:
        """Return copy of current guesses.

        Returns:
          List[Guess]: Deep copy of list of guesses

        """
        return deepcopy(self._guesses)

    def game_state(self) -> GameState:
        """Determine state of game.

        Returns:
          GameState: The state of the game

        """
        finalized_guesses: int = 0

        for guess in self._guesses:
            if not guess.is_final:
                continue

            finalized_guesses += 1
            if all(
                li.value == LetterInfo.IN_WORD_RIGHT_POSITION.value
                for _, li in guess.compare_to_word(self._word)
            ):
                return GameState.WON

        if finalized_guesses == len(self._guesses):
            return GameState.LOSE

        return GameState.PLAYING

    @property
    def word(self) -> str:
        """Get word

        Returns:
          str: The word

        """
        return self._word
