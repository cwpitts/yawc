"""Guess model

What does a guess need to track?
- Current state (buffer)
- Whether or not it's been submitted

"""
from enum import Enum, auto
from itertools import zip_longest
from typing import List, Tuple

import pyray


class LetterInfo(Enum):
    """LetterInfo represents the validity/position of a letter.

    A letter can be in one of three states:
    - In the word, but in the wrong position
    - In the word and in the right position
    - Not in the word

    """

    IN_WORD_RIGHT_POSITION = 1
    IN_WORD_WRONG_POSITION = 2
    NOT_IN_WORD = 3

    def console_repr(self):
        if self.value == LetterInfo.NOT_IN_WORD.value:
            return "[]"
        if self.value == LetterInfo.IN_WORD_WRONG_POSITION.value:
            return "[x]"
        if self.value == LetterInfo.IN_WORD_RIGHT_POSITION.value:
            return "[!]"

    def console_color(self):
        if self.value == LetterInfo.NOT_IN_WORD.value:
            return pyray.GRAY
        if self.value == LetterInfo.IN_WORD_WRONG_POSITION.value:
            return pyray.YELLOW
        if self.value == LetterInfo.IN_WORD_RIGHT_POSITION.value:
            return pyray.GREEN

    def __gt__(self, o: "LetterInfo"):
        return self.value > o.value

    def __lt__(self, o: "LetterInfo"):
        return self.value < o.value


class Guess:
    def __init__(self, size: int):
        """The Guess class.

        A guess is a list of letters that comprise a word.

        Args:
          size (int): Integer size of word

        """
        self._size: int = size
        self._buffer: List[str] = []
        self._is_final: bool = False

    def compare_to_word(self, word: str) -> List[Tuple[str, LetterInfo]]:
        """Compare guess to word.

        Args:
          word (str): Word to compare with

        Returns:
          info (List[LetterInfo]):
            List of information about each letter in the guess

        """
        info: List[Tuple[str, LetterInfo]] = []

        for expected, actual in zip_longest(word, self._buffer, fillvalue=""):
            if expected == actual:
                info.append((actual, LetterInfo.IN_WORD_RIGHT_POSITION))
            elif actual != "" and actual in word:
                info.append((actual, LetterInfo.IN_WORD_WRONG_POSITION))
            else:
                info.append((actual, LetterInfo.NOT_IN_WORD))

        return info

    def add_to_buffer(self, letter: str):
        """Add new letter to buffer

        Args:
          letter (str): Letter to add

        Notes:
          This function is equivalent to a no-op if the buffer is
          full.

        """
        if self._is_final:
            return

        if len(self._buffer) == self._size:
            return

        self._buffer.append(letter.upper())

    def trim_buffer(self):
        """Trim word from buffer.

        """
        if self._is_final:
            return
        if len(self._buffer) == 0:
            return

        self._buffer.pop(-1)

    @property
    def is_final(self) -> bool:
        return self._is_final

    def finalize(self):
        """Finalize a guess.

        Sets the guess to be finalized, which disables all further
        mutation.

        """
        if len(self._buffer) != self._size:
            return
        self._is_final = True

    @property
    def size(self) -> int:
        """Get word size.

        Returns:
          int: Size of word
        """
        return self._size

    def buffer_str(self) -> str:
        """Get current buffer contents as a string

        Returns:
          str: Buffer contents rendered as string

        """
        return "".join(self._buffer)
