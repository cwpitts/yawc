from typing import Any

import pytest
import yawc.__main__ as main


@pytest.mark.parametrize("name", ["/foo/bar", "bar", "barfoo.png"])
def test_parse_args_should_reject_invalid_wordlists(name: str):
    with pytest.raises(SystemExit):
        main.parse_args(["-W", name])


@pytest.mark.parametrize("name", ["simple", "default", "ogden", "lawler"])
def test_parse_args_should_accept_valid_wordlists(name: str):
    main.parse_args(["-W", name])


@pytest.mark.parametrize("length", ["string", 2.3, ["adelie", "gentoo"], -1, 0])
def test_parse_args_should_reject_invalid_word_lengths(length: Any):
    with pytest.raises(SystemExit):
        main.parse_args(["--word-length", str(length)])


@pytest.mark.parametrize("length", [2, 3, 4, 5])
def test_parse_args_should_accept_valid_word_lengths(length: int):
    main.parse_args(["--word-length", str(length)])


@pytest.mark.parametrize("level", ["CRITICAL", "FOOBAR", "BARFOO", 123])
def test_parse_args_should_reject_invalid_log_levels(level: Any):
    with pytest.raises(SystemExit):
        main.parse_args(["-L", str(level)])


@pytest.mark.parametrize("level", ["INFO", "DEBUG"])
def test_parse_args_should_accept_valid_log_levels(level: str):
    main.parse_args(["-L", level])


@pytest.mark.parametrize(
    "wordlist, should_validate", [("simple", True), ("barfoo", False), ("ogden", True)]
)
def test_existing_wordlist(wordlist, should_validate):
    if should_validate:
        main.existing_wordlist(wordlist)
    else:
        with pytest.raises(ValueError):
            main.existing_wordlist(wordlist)
