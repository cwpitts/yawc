from typing import List

import pytest
from yawc.model.guess import Guess, LetterInfo


@pytest.mark.parametrize(
    "buf, word, expected",
    [
        (
            ["a", "d", "e", "l", "i", "e"],
            "adelie",
            [
                ("a", LetterInfo.IN_WORD_RIGHT_POSITION),
                ("d", LetterInfo.IN_WORD_RIGHT_POSITION),
                ("e", LetterInfo.IN_WORD_RIGHT_POSITION),
                ("l", LetterInfo.IN_WORD_RIGHT_POSITION),
                ("i", LetterInfo.IN_WORD_RIGHT_POSITION),
                ("e", LetterInfo.IN_WORD_RIGHT_POSITION),
            ],
        ),
        (
            ["t", "p"],
            "gentoo",
            [
                ("t", LetterInfo.IN_WORD_WRONG_POSITION),
                ("p", LetterInfo.NOT_IN_WORD),
                ("", LetterInfo.NOT_IN_WORD),
                ("", LetterInfo.NOT_IN_WORD),
                ("", LetterInfo.NOT_IN_WORD),
                ("", LetterInfo.NOT_IN_WORD),
            ],
        ),
    ],
)
def test_compare_word(buf: List[str], word: str, expected: List[LetterInfo]):
    g: Guess = Guess(len(word))
    g._buffer = buf

    assert g.compare_to_word(word) == expected


@pytest.mark.parametrize("letter", ["a", "b", "d", "z", "P"])
def test_add_to_buffer_should_add_to_buffer(letter: str):
    g: Guess = Guess(1)
    g.add_to_buffer(letter)
    assert letter.upper() in g._buffer


def test_add_to_buffer_should_limit_size():
    g: Guess = Guess(6)
    g._buffer = ["A", "D", "E", "L", "I", "E"]
    g.add_to_buffer("F")
    assert "F" not in g._buffer


def test_trim_buffer_should_not_pop_empty_buffer():
    g: Guess = Guess(2)
    g.trim_buffer()


def test_trim_buffer_should_trim_letter():
    g: Guess = Guess(5)
    g._buffer = ["R", "O", "C", "K", "H", "O", "P", "P", "E", "R"]
    g.trim_buffer()
    assert g._buffer[-1] == "E"
