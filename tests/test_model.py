import tempfile
from pathlib import Path
from typing import List

import pytest
from yawc import constants
from yawc.model.guess import Guess
from yawc.model.model import GameState, Model


def make_final_guess(size: int) -> Guess:
    g: Guess = Guess(size)
    for _ in range(size):
        g.add_to_buffer("A")
    g.finalize()
    return g


@pytest.mark.parametrize(
    "wordlist, word_length, expected_wordlist",
    [
        (["rockhopper", "adelie", "macaroni", "fairy"], 5, ["FAIRY"]),
        (["emperor", "king"], 3, []),
        (
            ["king", "emperor", "macaroni", "rockhopper", "gentoo", "adelie"],
            6,
            ["GENTOO", "ADELIE"],
        ),
    ],
)
def test__filter_words(wordlist: List[str], word_length: int, expected_wordlist: str):
    with tempfile.NamedTemporaryFile() as tmp_file:
        path: Path = Path(tmp_file.name)
        with open(path, "w") as out_file:
            for word in wordlist:
                out_file.write(f"{word}\n")

        assert sorted(Model._filter_words(None, path, word_length)) == sorted(
            expected_wordlist
        )


@pytest.mark.parametrize(
    "guesses, expected_state",
    [
        ([Guess(3), Guess(3)], GameState.PLAYING),
        (
            [make_final_guess(3), make_final_guess(3), make_final_guess(3), make_final_guess(3)],
            GameState.LOSE,
        ),
    ],
)
def test_game_state(guesses: List[Guess], expected_state: GameState):
    m: Model = Model(constants.DATA_DIR / "default.txt", word_length=5)
    m._guesses = guesses
    assert m.game_state() == expected_state
